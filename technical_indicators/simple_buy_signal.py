import json
import os

from api.yahoo import Yahoo
from screener.stock_screener import (
	GOLDEN_CROSS_50_200_FILE,
	MACD_CROSS_OVER_FILE,
	PRICE_CROSS_ABV_50_FILE,
	PRICE_BELOW_50_HIGH_FILE,
)
from utilities.html_manager import URLRequest
from utilities.utility import (
	OUTPUT_FOLDER,
	Utility
)


class SimpleBuySignal(URLRequest):
	@classmethod
	def check_signal(cls, symbol):
		cls.golden_cross_50_200 = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, GOLDEN_CROSS_50_200_FILE)))
		cls.macd_cross_over = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, MACD_CROSS_OVER_FILE)))

		row = [symbol, Yahoo.get_price(symbol)]

		# technical indicator
		is_golden_cross_50_200 = 1 if symbol in cls.golden_cross_50_200 else 0
		is_macd_cross_over = 1 if symbol in cls.macd_cross_over else 0

		row.extend([
			is_macd_cross_over, is_golden_cross_50_200
		])
		return row

	@classmethod
	def has_buy_signal(cls, row):
		return row[2] or row[3]

	@classmethod
	def get_header(cls):
		return [
			'SYMBOL', 'Price', 'MACD X', 'GC 50-200',
		]

	@classmethod
	def get_urls(cls):
		return []
