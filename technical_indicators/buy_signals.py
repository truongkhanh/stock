import json
import os

from api.reuters import ReuterRank
from api.stockta import (
	StockTAAnalysis,
	StockTAMACD,
	StockTARSI,
)
from api.zack import ZackRank
from screener.ibd_screener import (
	DEFAULT_NON_IBD_RANK,
	IBD50_MAP_FILE
)
from screener.stock_screener import (	
	GOLDEN_CROSS_20_50_FILE,
	GOLDEN_CROSS_50_200_FILE,
	MACD_CROSS_OVER_FILE,
	PRICE_CROSS_ABV_50_FILE,
	PRICE_BELOW_50_HIGH_FILE,
	STOCHASTIC_OVER_SOLD_FILE,
)
from api.barchart import AveragePrice
from utilities.html_manager import URLRequest
from utilities.utility import (
	OUTPUT_FOLDER,
	Utility
)


class BuySignal(URLRequest):
	@classmethod
	def check_signal(cls, symbol):
		cls.ibd_list = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, IBD50_MAP_FILE)))
		cls.price_cross_abv_50 = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, PRICE_CROSS_ABV_50_FILE)))
		cls.price_below_50_high = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, PRICE_BELOW_50_HIGH_FILE)))
		cls.golden_cross_20_50 = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, GOLDEN_CROSS_20_50_FILE)))
		cls.golden_cross_50_200 = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, GOLDEN_CROSS_50_200_FILE)))
		cls.macd_cross_over = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, MACD_CROSS_OVER_FILE)))
		cls.stochastic_over_sold = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, STOCHASTIC_OVER_SOLD_FILE)))

		row = [symbol]
		# rank
		price, avg_20d, avg_50d, avg_200d = AveragePrice.get_prices(symbol, [1, 20, 50, 200])
		ibd_rank = cls.ibd_list.get(symbol, DEFAULT_NON_IBD_RANK)
		zack_rank = ZackRank.get(symbol)
		reuter_rank = ReuterRank.get(symbol)
		stock_ta_info = StockTAAnalysis.get(symbol)
		row.extend([price, ibd_rank, zack_rank])


		# technical indicator
		is_above_20d = 1 if price > avg_20d else 0
		is_above_50d = 1 if price > avg_50d else 0
		is_above_200d = 1 if price > avg_200d else 0
		rsi = StockTARSI.get(symbol)
		macd_line, macd_signal_line = StockTAMACD.get(symbol)
		is_macd_cross_over = 1 if symbol in cls.macd_cross_over else 0
		is_stochastic_over_sold = 1 if symbol in cls.stochastic_over_sold else 0
		is_price_cross_abv_50 = 1 if symbol in cls.price_cross_abv_50 else 0
		is_price_below_50_high = 1 if symbol in cls.price_below_50_high else 0
		is_golden_cross_20_50 = 1 if symbol in cls.golden_cross_20_50 else 0
		is_golden_cross_50_200 = 1 if symbol in cls.golden_cross_50_200 else 0

		is_considered = 1 if is_macd_cross_over + is_golden_cross_50_200 else 0
		row.extend([
			is_macd_cross_over, is_stochastic_over_sold,
			is_price_cross_abv_50, is_price_below_50_high, is_golden_cross_20_50, is_golden_cross_50_200, is_considered
		])

		# not so important, put at the end
		row.extend(stock_ta_info)
		row.extend([reuter_rank, is_above_20d, is_above_50d, is_above_200d, rsi, macd_line, macd_signal_line])
		return row

	@classmethod
	def get_header(cls):
		return [
			'SYMBOL', 'Price',
			'IBD50 Rank', 'Zack Rank', 'MACD X', 'Stochastic Oversold',
			'Price X Abv 50d', 'Price Below 50d High', 'GC 20-50', 'GC 50-200', 'To Consider',
			'StockTA', 'StockTA EMA', 'StockTA MACD', 'Retuer Rank',
			'Abv SMA20', 'Abv SMA50', 'Abv SMA200', 'RSI', 'Fast MACD', 'Slow MACD',
		]

	@classmethod
	def get_urls(cls):
		url_request_classes = [
			ReuterRank,
			StockTAAnalysis,
			StockTAMACD,
			StockTARSI,
			ZackRank,
			AveragePrice
		]

		urls = []
		for url_request_class in url_request_classes:
			urls.extend(url_request_class.get_urls())
		return urls
