import csv
from datetime import datetime
import logging
import os
from Queue import Queue
from threading import Thread

from technical_indicators.simple_buy_signal import SimpleBuySignal
from technical_indicators.sell_signals import SellSignal
from utilities.utility import Utility


class StockCheckupThread(Thread):
	def __init__(self, signal_detector_class, symbol_queue, rows):
		"""
		Run signal_detector_class.check_signal and update rows.
		Params:
		symbol_queue: queue of pair (index, symbol).
		rows: list initialize with size as number of symbols.
		"""
		Thread.__init__(self)
		self.symbol_queue = symbol_queue
		self.rows = rows
		self.signal_detector_class = signal_detector_class

	def run(self):
		while not self.symbol_queue.empty():
			work = self.symbol_queue.get()  # fetch new work from the Queue
			print('{}/{}. {}'.format(str(len(self.rows) - self.symbol_queue.qsize()), str(len(self.rows)), work[1]))
			try:
				row = self.signal_detector_class.check_signal(work[1])
				self.rows[work[0]] = row  # Store data back at correct index
			except:
				logging.exception('exception_collect_data|symbol=%s', work[1])
			# signal to the queue that task has been processed
			self.symbol_queue.task_done()


class StockCheckup(object):
	@classmethod
	def check(cls, input_file, is_finding_sell_signal, is_finding_buy_signal, number_of_threads):
		symbols = Utility.get_symbols_from_file(input_file)
		signal_detector_classes = []
		if is_finding_buy_signal:
			signal_detector_classes.append(SimpleBuySignal)
		if is_finding_sell_signal:
			signal_detector_classes.append(SellSignal)
		all_rows = []

		for signal_detector_class in signal_detector_classes:
			# set up input and output
			queue = Queue(maxsize=0)
			for i, symbol in enumerate(symbols):
				queue.put((i, symbol))
			rows = [[symbol] for symbol in symbols]

			number_of_threads = min(number_of_threads, len(symbols))
			for i in range(number_of_threads):
				worker = StockCheckupThread(signal_detector_class, queue, rows)
				worker.setDaemon(True)
				worker.start()

			# wait until queue has been processed
			queue.join()
			all_rows.append(signal_detector_class.get_header())
			all_rows.extend(rows)
			all_rows.extend([[], []])  # add 2 new empty line between

		# write data
		output_file = cls._get_output_file_name(os.path.abspath(input_file))
		with open(output_file, 'w') as f:
			csv_writer = csv.writer(f, delimiter=',')
			csv_writer.writerows(all_rows)
		os.system('open {}'.format(output_file))

	@classmethod
	def _get_output_file_name(cls, input_file):
		folder_path = os.path.dirname(input_file)
		file_name = os.path.basename(input_file)
		now = datetime.now()
		output_file = "{}-{}.csv".format(os.path.splitext(file_name)[0], now.strftime("%Y-%m-%d"))
		output_file = os.path.join(folder_path, output_file)
		return output_file

	@classmethod
	def daily_buy_signal(cls, input_file):
		symbols = Utility.get_symbols_from_file(input_file)
		all_rows = []

		for symbol in symbols:
			row = SimpleBuySignal.check_signal(symbol)
			if SimpleBuySignal.has_buy_signal(row):
				all_rows.append(row)

		# write data
		if all_rows:
			output_file = cls._get_output_file_name(os.path.abspath(input_file))
			with open(output_file, 'w') as f:
				csv_writer = csv.writer(f, delimiter=',')
				csv_writer.writerows(all_rows)
			return output_file
		return None
