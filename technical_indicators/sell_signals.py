import json
import os

from api.stockta import StockTARSI
from api.zack import ZackRank
from utilities.html_manager import URLRequest
from screener.stock_screener import (
	MACD_CROSS_DOWN_FILE,
	STOCHASTIC_OVER_BOUGHT_FILE
)
from api.barchart import AveragePrice
from technical_indicators.climax_top_signal import ClimaxTopSignal
from utilities.utility import (
	OUTPUT_FOLDER,
	Utility
)


class SellSignal(URLRequest):
	OK_STATUS = ''
	KO_STATUS = 'X'

	@classmethod
	def check_signal(cls, symbol):
		cls.macd_cross_down = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, MACD_CROSS_DOWN_FILE)))
		cls.stochastic_over_bought = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, STOCHASTIC_OVER_BOUGHT_FILE)))

		row = [symbol]
		# Check average price
		price, avg_20d, avg_50d, avg_200d = AveragePrice.get_prices(symbol, [1, 20, 50, 200])
		row.append(price)
		zack_rank = ZackRank.get(symbol)
		row.append(SellSignal.OK_STATUS if zack_rank <= 3 else SellSignal.KO_STATUS)
		row.append(SellSignal.OK_STATUS if price > avg_20d else SellSignal.KO_STATUS)
		row.append(SellSignal.OK_STATUS if price > avg_50d else SellSignal.KO_STATUS)
		row.append(SellSignal.OK_STATUS if price > avg_200d else SellSignal.KO_STATUS)

		# RSI, MACD, Stochastic
		row.append(SellSignal.OK_STATUS if StockTARSI.get(symbol) < 70 else SellSignal.KO_STATUS)
		row.append(SellSignal.OK_STATUS if symbol not in cls.macd_cross_down else SellSignal.KO_STATUS)
		row.append(SellSignal.OK_STATUS if symbol not in cls.stochastic_over_bought else SellSignal.KO_STATUS)

		# Check climax top
		technical_sell = ClimaxTopSignal(symbol)
		check_sell_signal_funcs = [
			technical_sell.check_largest_daily_price_runup,
			technical_sell.check_heaviest_daily_volume,
			technical_sell.check_exhaustion_gap,
			technical_sell.check_climax_top_activity,
			technical_sell.check_railroad_track,
			technical_sell.check_distribution,
		]

		for check_sell_signal_func in check_sell_signal_funcs:
			check_result = check_sell_signal_func()
			if 'error' in check_result:
				row.append(check_result['date'].strftime('%d/%m'))
			else:
				row.append(SellSignal.OK_STATUS)

		total_fail = sum(1 for result in row[2:] if result != SellSignal.OK_STATUS)
		row.append(total_fail)
		return row

	@classmethod
	def get_header(cls):
		return [
			'SYMBOL', 'Price',
			'ZackRank',
			'Abv SMA 20',
			'Abv SMA 50',
			'Abv SMA 200',
			'RSI > 70',
			'MACD X Down',
			'Stochastic Overbought',
			'Largest Daily +',
			'Heaviest Daily Vol',
			'Exhaustion Gap',
			'Climax Top',
			'Railroad Track',
			'Distribution',
			'Total Signals',
		]

	@classmethod
	def get_urls(cls):
		url_request_classes = [
			StockTARSI,
			ZackRank,
			AveragePrice,
		]

		urls = []
		for url_request_class in url_request_classes:
			urls.extend(url_request_class.get_urls())
		return urls
