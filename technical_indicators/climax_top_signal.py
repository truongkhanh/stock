from api.yahoo import Yahoo
from utilities.utility import exception_decorator


class ClimaxTopSignal:
	NUM_DAYS_TO_CHECK = 10
	NUM_WEEK_TO_CHECK = 2

	def __init__(self, symbol):
		price_info = Yahoo.get_price_history(symbol)
		self.avg_volume = price_info['avg_volume']
		self.daily_prices = price_info['daily_prices']
		self.weekly_prices = price_info['weekly_prices']

		if len(self.daily_prices) < ClimaxTopSignal.NUM_DAYS_TO_CHECK:
			raise Exception('daily_history_not_sufficient')

	def check_largest_daily_price_runup(self):
		"""Check a day with larger price increase than on any previous up day in daily history"""
		LIMIT_TIMES_MAX_RECENT_LOWER_PAST = 1

		max_recent_increase = -100
		max_recent_increase_date = None
		for i in range(ClimaxTopSignal.NUM_DAYS_TO_CHECK):
			if max_recent_increase < self.daily_prices[i]['change']:
				max_recent_increase = self.daily_prices[i]['change']
				max_recent_increase_date = self.daily_prices[i]['date']
		count_times_max_recent_lower_past = 0
		for daily_price_info in self.daily_prices[ClimaxTopSignal.NUM_DAYS_TO_CHECK:]:
			if daily_price_info['change'] > max_recent_increase:
				count_times_max_recent_lower_past += 1
				if count_times_max_recent_lower_past > LIMIT_TIMES_MAX_RECENT_LOWER_PAST:
					break

		if count_times_max_recent_lower_past <= LIMIT_TIMES_MAX_RECENT_LOWER_PAST:
			return {
				'error': 1,
				'date': max_recent_increase_date
			}
		else:
			return {}

	def check_heaviest_daily_volume(self):
		"""check a day with heaviest volume day"""
		LIMIT_TIMES_MAX_RECENT_LOWER_PAST = 1

		max_recent_volume = 0
		max_recent_volume_date = None
		for i in range(ClimaxTopSignal.NUM_DAYS_TO_CHECK):
			if max_recent_volume < self.daily_prices[i]['volume']:
				max_recent_volume = self.daily_prices[i]['volume']
				max_recent_volume_date = self.daily_prices[i]['date']

		count_times_max_recent_lower_past = 0
		for daily_price_info in self.daily_prices[ClimaxTopSignal.NUM_DAYS_TO_CHECK:]:
			if daily_price_info['volume'] > max_recent_volume:
				count_times_max_recent_lower_past += 1
				if count_times_max_recent_lower_past > LIMIT_TIMES_MAX_RECENT_LOWER_PAST:
					break

		if count_times_max_recent_lower_past <= LIMIT_TIMES_MAX_RECENT_LOWER_PAST:
			return {
				'error': 1,
				'date': max_recent_volume_date
			}
		return {}

	def check_exhaustion_gap(self):
		"""Check price advancing rapidly and then open on gap up and the whole day stay above open price"""
		if not self._is_running_up_rapidly(0):
			return {}

		gap_up_day_indexes = self._get_gap_up_day_indexes()
		if gap_up_day_indexes:
			return {
				'error': 1,
				'date': self.daily_prices[gap_up_day_indexes[0]]['date']
			}
		return {}

	def _is_advancing_rapidly(self):
		"""Check price increase 10%+ in daily history"""
		return self.daily_prices[0]['close'] > 1.1 * self.daily_prices[-1]['close']

	def _get_gap_up_day_indexes(self):
		"""Return list of indexes in self.daily_prices where opening price is 1% gap up at least"""
		return [i for i in range(ClimaxTopSignal.NUM_DAYS_TO_CHECK) if self._is_exhaustion_gap_up_day(i)]

	def _is_exhaustion_gap_up_day(self, day_index):
		is_gap_up = self.daily_prices[day_index]['open'] > self.daily_prices[day_index + 1]['close'] * 1.01
		is_increase = self.daily_prices[day_index]['change'] > 1
		is_maintain_gap_up = self.daily_prices[day_index]['low'] > self.daily_prices[day_index + 1]['close'] + 0.5 * (self.daily_prices[day_index]['open'] - self.daily_prices[day_index + 1]['close'])
		return is_gap_up and is_increase and is_maintain_gap_up

	def check_climax_top_activity(self):
		"""Check if a price up week low higher than close of previous week"""
		for i in range(ClimaxTopSignal.NUM_WEEK_TO_CHECK):
			if self._is_climax_top(i):
				return {
					'error': 1,
					'date': self.weekly_prices[i]['date']
				}
		return {}

	def _is_climax_top(self, week_index):
		is_low_almost_above_previous_close = self.weekly_prices[week_index]['low'] >= 0.995 * self.weekly_prices[week_index + 1]['close']
		is_up = self.weekly_prices[week_index]['change'] > 2
		is_running_up = self._is_running_up_rapidly(week_index)
		return is_low_almost_above_previous_close and is_up and is_running_up

	@exception_decorator(False)
	def _is_running_up_rapidly(self, week_index):
		"""Having at least 5 weeks price up in last 6 weeks or 4 week continuous"""
		count_price_up = 0
		for i in range(week_index, week_index + 4):
			if self.weekly_prices[i]['change'] > 0:
				count_price_up += 1
		if count_price_up == 4:
			return True

		count_price_up = 0
		for i in range(week_index, week_index + 6):
			if self.weekly_prices[i]['change'] > 0:
				count_price_up += 1
		return count_price_up >= 5

	def check_railroad_track(self):
		"""Check if a week low/high range belong previous low/high range"""
		for i in range(ClimaxTopSignal.NUM_WEEK_TO_CHECK):
			is_previous_climax_top = self._is_climax_top(i + 1)
			is_high_almost_below_previous_high = self.weekly_prices[i]['high'] <= 1.005 * self.weekly_prices[i + 1]['high']
			is_low_almost_above_previous_low = self.weekly_prices[i]['low'] >= 0.995 * self.weekly_prices[i + 1]['low']
			is_down = self.weekly_prices[i]['change'] < 0
			if is_previous_climax_top and is_high_almost_below_previous_high and is_low_almost_above_previous_low and is_down:
				return {
					'error': 1,
					'date': self.weekly_prices[i]['date']
				}
		return {}

	def check_distribution(self):
		for i in range(ClimaxTopSignal.NUM_DAYS_TO_CHECK):
			is_volume_high = self.daily_prices[i]['volume'] > 1.3 * self.avg_volume
			is_price_change_low = self.daily_prices[i]['change'] < 1
			if is_volume_high and is_price_change_low:
				return {
					'error': 1,
					'date': self.daily_prices[i]['date']
				}
		return {}
