import unittest

from api.barchart import AveragePrice


class AveragePriceTest(unittest.TestCase):
    def test_get_prices(self):
        symbol = 'BIDU'
        duration = [20, 50, 200]
        prices = AveragePrice.get_prices(symbol, duration)
        print(prices)
        self.assertTrue(all(prices))


if __name__ == '__main__':
    unittest.main()