Copy code to server
rsync -avz --cvs-exclude `pwd`/ --exclude="*.idea" --exclude="*.csv" --exclude="*.html" --exclude=".git" awshost1:/home/ubuntu/stock_research

cron job set up
0 23 * * * python ~/stock_research/new_strategy.py -i data/ibd.txt