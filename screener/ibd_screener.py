from datetime import date, datetime, timedelta
import json
import os

from api.ibd50 import IBD50
from utilities.html_manager import URLRequest
from utilities.utility import Utility, OUTPUT_FOLDER

IBD50_MAP_FILE = 'ibd50.json'  # Map ibd 50 rank to symbol
IBD_LIST_FILE = 'ibd.txt'  # List of all symbols used to appear in ibd 50 list
IBD_HISTORY_FILE = 'ibd_history.json'  # Symbol to list of ibd rank history
DEFAULT_NON_IBD_RANK = 51


class IBDScreener(URLRequest):
	@classmethod
	def write_ibd(cls):
		"""Write to json, map from symbol to list of ibd index history"""
		ibd50 = IBD50.get_ibd_list()
		# update list of ibd stock
		cls.update_ibd_list(ibd50)
		# update ibd rank history
		cls.update_ibd_history(ibd50)
		# update current ibd rank list for screener
		Utility.write_file(os.path.join(OUTPUT_FOLDER, IBD50_MAP_FILE), json.dumps(ibd50))

	@classmethod
	def update_ibd_list(cls, ibd50):
		"""Mantain txt file list of all symbols used to appear on ibd50"""
		symbols = Utility.get_symbols_from_file(os.path.join(OUTPUT_FOLDER, IBD_LIST_FILE))

		new_ibd_stocks = []
		for i in range(IBD50.NUM_IBD_STOCK):
			rank = i + 1
			symbol = ibd50.get(rank)
			if symbol and symbol not in symbols:
				new_ibd_stocks.append(symbol)
		if new_ibd_stocks:
			print('{} new stocks added to IBD List'.format(len(new_ibd_stocks)))
		symbols.extend(new_ibd_stocks)
		symbols.sort()
		Utility.write_file(os.path.join(OUTPUT_FOLDER, IBD_LIST_FILE), '\n'.join(symbols))

	@classmethod
	def update_ibd_history(cls, ibd50):
		"""Maintain json file map from symbol to list of ibd rank history
		time_run: a key added to store last run date
		"""
		ibd_history = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, IBD_HISTORY_FILE), default_value='{}'))
		time_run = ibd_history.get('time_run', [])
		if not cls._can_run_(time_run):
			return

		time_run.append(date.today().strftime('%Y-%m-%d'))

		symbol_history = ibd_history.get('symbol_history', {})
		# add new rank for old symbol
		for symbol in symbol_history:
			symbol_history[symbol].append(ibd50.get(symbol, DEFAULT_NON_IBD_RANK))
		# add new rank for new symbol
		for i in range(IBD50.NUM_IBD_STOCK):
			rank = i + 1
			symbol = ibd50.get(rank)
			if symbol and symbol not in symbol_history:
				symbol_history[symbol] = [ibd50[symbol]]

		ibd_history['time_run'] = time_run
		ibd_history['symbol_history'] = symbol_history
		Utility.write_file(os.path.join(OUTPUT_FOLDER, IBD_HISTORY_FILE), json.dumps(ibd_history))

	@classmethod
	def _can_run_(cls, time_run):
		"""Only allow to run on the next working day of the last run"""
		if not time_run:
			return True
		datetime_obj = datetime.strptime(time_run[-1], '%Y-%m-%d').date()
		weekday = datetime_obj.weekday()
		duration_in_days = 1
		if weekday == 4:
			duration_in_days = 3
		elif weekday == 5:
			duration_in_days = 2
		datetime_obj += timedelta(days=duration_in_days)
		return date.today() >= datetime_obj
