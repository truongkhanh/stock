from datetime import date, datetime
import json
import os
from threading import Thread

from screener.ibd_screener import IBDScreener
from api.finviz import FinvizScreener
from api.stock_screener_org import StockScreenerOrg
from utilities.html_manager import URLRequest
from utilities.utility import Utility, OUTPUT_FOLDER

PRICE_CROSS_ABV_50_FILE = 'price_cross_abv_50.json'
PRICE_BELOW_50_HIGH_FILE = 'price_below_50_high.json'
GOLDEN_CROSS_20_50_FILE = 'golden_cross_20_50.json'
GOLDEN_CROSS_50_200_FILE = 'golden_cross_50_200.json'
MACD_CROSS_OVER_FILE = 'macd_cross_over.json'
MACD_CROSS_DOWN_FILE = 'macd_cross_down.json'
STOCHASTIC_OVER_BOUGHT_FILE = 'stochastic_over_bought.json'
STOCHASTIC_OVER_SOLD_FILE = 'stochastic_over_sold.json'
MAX_DAY_ALIVE = 14


class ScreenerThread(Thread):
	def __init__(self, output_file_name, func, func_arguments):
		Thread.__init__(self)
		self.output_file_name = output_file_name
		self.func = func
		self.func_arguments = func_arguments

	def run(self):
		"""Get list of symbols satisfying some patterns in the last MAX_DAY_ALIVE days"""
		symbols = self.func(*self.func_arguments)
		old_result = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, self.output_file_name), default_value='{}'))
		for symbol in symbols:
			# update latest time added
			old_result[symbol] = date.today().strftime('%Y-%m-%d')

		# delete old data
		for symbol in old_result.keys():
			time_added = datetime.strptime(old_result[symbol], '%Y-%m-%d').date()
			duration = (date.today() - time_added).days
			if duration >= MAX_DAY_ALIVE:
				del old_result[symbol]
		Utility.write_file(os.path.join(OUTPUT_FOLDER, self.output_file_name), json.dumps(old_result))


class Screener(URLRequest):
	@classmethod
	def screen(cls, is_run_ibd=False):
		if is_run_ibd:
			# run first because it is not completely automatic
			IBDScreener.write_ibd()

		tasks = [
			# [PRICE_CROSS_ABV_50_FILE, FinvizScreener.get, FinvizScreener.PRICE_CROSS_ABV_50_OPT],
			# [PRICE_BELOW_50_HIGH_FILE, FinvizScreener.get, FinvizScreener.PRICE_BELOW_50_HIGH],
			# [GOLDEN_CROSS_20_50_FILE, FinvizScreener.get, FinvizScreener.GOLDEN_CROSS_20_50_OPT],
			[GOLDEN_CROSS_50_200_FILE, FinvizScreener.get, FinvizScreener.GOLDEN_CROSS_50_200_OPT],
			[MACD_CROSS_OVER_FILE, StockScreenerOrg.get_macd_cross_over],
			# [MACD_CROSS_DOWN_FILE, StockScreenerOrg.get_macd_cross_down],
			# [STOCHASTIC_OVER_BOUGHT_FILE, StockScreenerOrg.get_stochastic_over_bought],
			# [STOCHASTIC_OVER_SOLD_FILE, StockScreenerOrg.get_stochastic_over_sold]
		]

		workers = []
		for task in tasks:
			worker = ScreenerThread(task[0], task[1], task[2:])
			worker.setDaemon(True)
			workers.append(worker)
			worker.start()

		for worker in workers:
			worker.join()

	@classmethod
	def get_urls(cls):
		url_request_classes = [
			FinvizScreener,
			StockScreenerOrg
		]

		urls = []
		for url_request_class in url_request_classes:
			urls.extend(url_request_class.get_urls())
		return urls
