import logging
from Queue import Queue
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from threading import Thread
import time
import urllib2 as urllib


PHANTOM_PATH = '/Users/truongkhanh/Setup/phantomjs-2.1.1-macosx/bin/phantomjs'
INVESTORS_DOT_COM_HOMEPAGE = 'http://www.investors.com/'
TIME_OUT = 60
NUMBER_ATTEMPTS = 10


class HTMLManager:
	@classmethod
	def request_urls(cls, urls, number_of_threads=100):
		"""This function is used to request all urls parallel and cache the returned htmls"""
		url_queue = Queue(maxsize=len(urls))
		for url in urls:
			url_queue.put(url)

		number_of_threads = min(number_of_threads, len(urls))
		for i in range(number_of_threads):
			worker = URLRequestThread(url_queue)
			worker.setDaemon(True)
			worker.start()

		# wait until queue has been processed
		url_queue.join()

	@classmethod
	def get_html(cls, url, time_to_wait=0, support_javascript=False):
		for i in range(NUMBER_ATTEMPTS):
			try:
				if support_javascript:
					driver = webdriver.PhantomJS(executable_path=PHANTOM_PATH)
					driver.get(url)
					time.sleep(time_to_wait)
					elem = driver.find_element_by_xpath('//*')
					html = elem.get_attribute('outerHTML')
					driver.close()
					return html
				else:
					return urllib.urlopen(url, timeout=TIME_OUT).read()
			except:
				pass
		raise Exception('fail_to_request_url|url={}'.format(url))

	@classmethod
	def get_investors_urls(cls, symbols, map_symbol_to_investors_url={}):
		"""Return the url of symbols on investors.com"""
		driver = webdriver.Chrome()
		for i, symbol in enumerate(symbols):
			try:
				print('{}/{}.{}'.format(i + 1, len(symbols), symbol))
				if symbol in map_symbol_to_investors_url:
					continue
				driver.get(INVESTORS_DOT_COM_HOMEPAGE)
				elements = driver.find_elements_by_class_name('topSearch')
				elem = elements[1]
				elem.send_keys(symbol)
				elem.send_keys(Keys.RETURN)
				time.sleep(1)
				map_symbol_to_investors_url[symbol] = driver.current_url
			except:
				logging.exception('fail_to_get_investors_dot_com_url|symbol=%s', symbol)
				pass
		return map_symbol_to_investors_url


class URLRequestThread(Thread):
	def __init__(self, url_queue):
		Thread.__init__(self)
		self.url_queue = url_queue

	def run(self):
		while not self.url_queue.empty():
			url = self.url_queue.get()  # fetch new work from the Queue
			try:
				HTMLManager.get_html(url)
			except:
				logging.exception('fail_to_request|url=%s', url)
			# signal to the queue that task has been processed
			self.url_queue.task_done()


class URLRequest:
	@classmethod
	def get_urls(cls):
		"""Return list of urls requested during the program.
		The url should only contain one parameter which is the symbol marked as {}
		"""
		return []

	@classmethod
	def get_urls_requested_on_symbols(cls, urls, symbols=[]):
		full_urls = []
		for url in urls:
			if '{}' in url:
				full_urls.extend([url.format(symbol) for symbol in symbols])
			else:
				full_urls.append(url)
		return full_urls
