import codecs
import json
import os

from utilities.html_manager import HTMLManager
import logging

OUTPUT_FOLDER = 'data'
INVESTORS_URLS = 'investors_urls.json'


class Utility:
	@staticmethod
	def is_float(input):
		try:
			float(input.replace(',', ''))
		except ValueError:
			return False

		return True

	@staticmethod
	def to_float(input):
		try:
			return float(input.replace(',', ''))
		except:
			return 0

	@staticmethod
	def is_int(input):
		try:
			int(input.replace(',', ''))
		except ValueError:
			return False

		return True

	@staticmethod
	def to_int(input):
		return int(input.replace(',', ''))

	@staticmethod
	def read_file(file_name, default_value=''):
		if os.path.exists(file_name):
			with codecs.open(file_name, encoding='utf-8') as f:
				content = f.read()
				if content:
					return content
		return default_value

	@staticmethod
	def write_file(file_name, content):
		with open(file_name, 'w') as f:
			return f.write(content)

	@staticmethod
	def get_symbols_from_file(file_name):
		file_content = Utility.read_file(file_name)
		return sorted(set(file_content.split()))

	@staticmethod
	def generate_html_page(symbol_list_file):
		"""Update dashboard.html containing symbol and urls to stockcharts.com and finviz.com"""
		symbols = Utility.get_symbols_from_file(symbol_list_file)
		investors_urls = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, INVESTORS_URLS), default_value={}))
		rows_html = []
		for symbol in symbols:
			row_html = Utility._generate_row(symbol, investors_urls)
			rows_html.extend(row_html)

		html_file = os.path.join(OUTPUT_FOLDER, 'template.html')
		template_content = Utility.read_file(html_file)
		new_file_content = template_content.replace('<NEW-ROWS>', '\n'.join(rows_html))
		symbol_list_file_name_no_ext = os.path.splitext(os.path.basename(symbol_list_file))[0]
		output_file = os.path.join(OUTPUT_FOLDER, 'dashboard-{}.html'.format(symbol_list_file_name_no_ext))
		Utility.write_file(output_file, new_file_content)
		return output_file

	@staticmethod
	def _generate_row(symbol, investors_urls):
		return [
			'<tr>',
			'<td>{}</td>'.format(symbol),
			'<td><a href="{}" target="_blank">Investors</a></td>'.format(investors_urls.get(symbol)),
			'<td><a href="http://stockcharts.com/h-sc/ui?s={}" target="_blank">Stockcharts</a></td>'.format(symbol),
			'<td><a href="http://finviz.com/quote.ashx?t={}" target="_blank">Finvz</a></td>'.format(symbol),
			'<td><a href="https://www.zacks.com/stock/research/{}/earnings-announcements" target="_blank">Zack Earning</a></td>'.format(symbol),
			'<td><a href="https://www.zacks.com/stock/quote/{}/income-statement" target="_blank">Zack Income</a></td>'.format(symbol),
			'</tr>',
		]

	@classmethod
	def get_investors_urls(cls, symbol_list_file):
		"""Return the url of symbols on investors.com"""
		symbols = Utility.get_symbols_from_file(symbol_list_file)
		map_symbol_to_investors_url = json.loads(Utility.read_file(os.path.join(OUTPUT_FOLDER, INVESTORS_URLS), default_value={}))
		HTMLManager.get_investors_urls(symbols, map_symbol_to_investors_url)
		Utility.write_file(os.path.join(OUTPUT_FOLDER, INVESTORS_URLS), json.dumps(map_symbol_to_investors_url))


def exception_decorator(default_value=None):
	def _outer_wrapper(wrapped_function):
		def _wrapper(*args, **kwargs):
			try:
				return wrapped_function(*args, **kwargs)
			except:
				logging.exception('exception|func=%s,args=%s,kwargs=%s', wrapped_function, args, kwargs)
				return default_value
		return _wrapper

	return _outer_wrapper
