import smtplib, os, codecs

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText


class EmailHelper:
    @staticmethod
    def sendEmail(toEmails, subject, body, txtAttachPaths):
        fromEmail = 'Truong Khanh'
        username = 'truongkhanhit1'
        password = 'fusion360'

        msg = MIMEMultipart()
        s = smtplib.SMTP('smtp.gmail.com:587')
        s.starttls()
        s.login(username, password)

        msg['Subject'] = subject
        msg['From'] = fromEmail
        body = body
        content = MIMEText(body, 'plain')
        msg.attach(content)

        for txtAttachPath in txtAttachPaths:
            if os.path.isfile(txtAttachPath):
                with codecs.open(txtAttachPath, "r", encoding="utf-8") as f:
                    attachment = MIMEText(f.read())
                    attachment.add_header('Content-Disposition', 'attachment', filename=os.path.basename(txtAttachPath))
                    msg.attach(attachment)
        s.sendmail(fromEmail, toEmails, msg.as_string())
        s.quit()
