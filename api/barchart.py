from bs4 import BeautifulSoup

from utilities.html_manager import (
	HTMLManager,
	URLRequest
)
from utilities.utility import Utility


class AveragePrice(URLRequest):
	URL = 'https://www.barchart.com/stocks/quotes/{}/technical-analysis'

	@classmethod
	def get_price(cls, symbol):
		html = HTMLManager.get_html(cls.URL.format(symbol))
		bs_obj = BeautifulSoup(html, "html.parser")
		return cls._get_price(bs_obj)

	@classmethod
	def get_prices(cls, symbol, durations):
		html = HTMLManager.get_html(cls.URL.format(symbol))
		bs_obj = BeautifulSoup(html, "html.parser")
		result = []
		for duration in durations:
			if duration == 1:
				result.append(cls._get_price(bs_obj))
			elif duration == 20:
				result.append(cls._get_price_on_rowth(bs_obj, 2))
			elif duration == 50:
				result.append(cls._get_price_on_rowth(bs_obj, 3))
			elif duration == 200:
				result.append(cls._get_price_on_rowth(bs_obj, 5))
			else:
				raise ValueError('invalid_duration|duratoin=%s', duration)
		return result

	@classmethod
	def is_50d_200d_near(cls, symbol):
		try:
			html = HTMLManager.get_html(cls.URL.format(symbol))
			bs_obj = BeautifulSoup(html, "html.parser")
			price_50d = cls._get_price_on_rowth(bs_obj, 3)
			price_200d = cls._get_price_on_rowth(bs_obj, 5)
			return abs(price_50d - price_200d) / Utility.to_float(price_50d + price_200d) < 0.02
		except:
			return False

	@classmethod
	def _get_price(cls, bs_obj):
		price = Utility.to_float(bs_obj.find('span', {'id': 'dtaLast'}).text.strip())
		return price

	@classmethod
	def _get_price_on_rowth(cls, bs_obj, row_index):
		try:
			table = bs_obj.find('div', {'class': 'analysis-table-wrapper bc-table-wrapper'}).find('table')
			row = table.findAll('tr')[row_index]
			avg_price = Utility.to_float(row.findAll('td')[1].text.strip())
			return avg_price
		except:
			return 0

	@classmethod
	def get_urls(cls):
		return [cls.URL]
