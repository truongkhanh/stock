from bs4 import BeautifulSoup

from utilities.html_manager import (
	HTMLManager,
	URLRequest
)
from utilities.utility import Utility
import logging


class StockTAAnalysis(URLRequest):
	URL = 'http://www.stockta.com/cgi-bin/analysis.pl?symb={}&cobrand=&mode=stock'

	@classmethod
	def get(cls, symbol):
		try:
			html = HTMLManager.get_html(cls.URL.format(symbol))
			bs_obj = BeautifulSoup(html, "html.parser")

			result = [cls.add_escape_excel(cls.get_analysis(bs_obj))]
			result.extend([cls.add_escape_excel(rating) for rating in cls.get_ema_macd(bs_obj)])
			return result
		except:
			return ['', '', '']

	@classmethod
	def add_escape_excel(cls, rating):
		return "'{}'".format(rating)

	@classmethod
	def get_analysis(cls, bs_obj):
		table = bs_obj.findAll('table', {'class': 'borderTable'})[1]
		analysis_row = table.findAll('tr')[1]
		columns = analysis_row.findAll('td')[1:]

		result = ''
		for column in columns:
			value = column.text.strip()
			result += cls.encode_ratio(value)
		return result

	@classmethod
	def get_ema_macd(cls, bs_obj):
		chart_indicator_table = bs_obj.findAll('table', {'class': 'borderTable'})[5]
		rows = chart_indicator_table.findAll('tr')
		ema = cls.parse_ema_macd_value(rows[1])
		macd = cls.parse_ema_macd_value(rows[2])
		return [ema, macd]

	@classmethod
	def parse_ema_macd_value(cls, row):
		columns = row.findAll('td')[1:]

		result = ''
		for column in columns:
			value = column.text.strip()
			result += cls.encode_chart_indicator(value)
		return result

	@classmethod
	def encode_ratio(cls, ratio):
		if 'Neutral' in ratio:
			return '0'
		elif 'Bullish' in ratio:
			return '+'
		else:
			return '-'

	@classmethod
	def encode_chart_indicator(cls, indicator):
		if 'VBu' == indicator or 'Bu' == indicator:
			return '+'
		elif 'N' == indicator:
			return '0'
		elif 'Be' == indicator or 'VBe' == indicator:
			return '-'
		else:
			logging.exception('stockta_wrong_indicator|indicator=%s', indicator)
			raise

	@classmethod
	def get_urls(cls):
		return [cls.URL]


class StockTARSI(URLRequest):
	URL = 'http://www.stockta.com/cgi-bin/analysis.pl?symb={}&mode=table&table=rsi'
	DEFAULT_RSI = 0

	@classmethod
	def get(cls, symbol):
		try:
			html = HTMLManager.get_html(cls.URL.format(symbol))
			bs_obj = BeautifulSoup(html, "html.parser")
			table = bs_obj.find('table', {'class': 'borderTable'})
			rsi = table.find('tr').findAll('td')[1].find('font').text.strip()
			return Utility.to_float(rsi)
		except:
			logging.exception('stockta_fail_to_read_rsi|symbol=%s', symbol)
			return StockTARSI.DEFAULT_RSI

	@classmethod
	def get_urls(cls):
		return [cls.URL]


class StockTAMACD(URLRequest):
	URL = 'http://www.stockta.com/cgi-bin/analysis.pl?symb={}&mode=table&table=macd'

	@classmethod
	def get(cls, symbol):
		try:
			html = HTMLManager.get_html(cls.URL.format(symbol))
			bs_obj = BeautifulSoup(html, "html.parser")
			table = bs_obj.find('table', {'class': 'borderTable'})
			columns = table.findAll('tr')[1].findAll('td')[1:3]

			result = []
			for column in columns:
				value = column.find('font').text.strip()
				result.append(Utility.to_float(value))
			return result
		except:
			logging.exception('stockta_failt_to_read_macd|symbol=%s', symbol)
			return [0, 0]

	@classmethod
	def get_urls(cls):
		return [cls.URL]
