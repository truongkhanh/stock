from bs4 import BeautifulSoup

from utilities.html_manager import (
	HTMLManager,
	URLRequest
)


class FinvizScreener(URLRequest):
	URL_20_50 = 'http://finviz.com/screener.ashx?v=411&f=ta_sma50_cross20b&ft=4&t={}'
	URL_50_200 = 'http://finviz.com/screener.ashx?v=411&f=ta_sma200_cross50b&ft=4&t={}'
	URL_PRICE_CROSS_ABV_50 = 'http://finviz.com/screener.ashx?v=411&f=ta_sma50_pca&ft=4&t={}'
	URL_PRICE_BELOW_50_HIGH = 'http://finviz.com/screener.ashx?v=411&f=ta_highlow50d_b10h,ta_highlow52w_a70h&ft=4&t={}'

	GOLDEN_CROSS_20_50_OPT = 1
	GOLDEN_CROSS_50_200_OPT = 2
	PRICE_CROSS_ABV_50_OPT = 3
	PRICE_BELOW_50_HIGH = 4

	@classmethod
	def get(cls, opt=GOLDEN_CROSS_20_50_OPT, symbols=''):
		"""Run screener.
		Params:
		symbols: if provided, screener on these symbol. Separated by comma
		"""
		if opt == FinvizScreener.GOLDEN_CROSS_20_50_OPT:
			url = cls.URL_20_50.format(symbols)
		elif opt == FinvizScreener.GOLDEN_CROSS_50_200_OPT:
			url = cls.URL_50_200.format(symbols)
		elif opt == FinvizScreener.PRICE_CROSS_ABV_50_OPT:
			url = cls.URL_PRICE_CROSS_ABV_50.format(symbols)
		else:
			url = cls.URL_PRICE_BELOW_50_HIGH.format(symbols)

		html = HTMLManager.get_html(url)
		bs_obj = BeautifulSoup(html, "html.parser")
		div = bs_obj.find('div', {'id': 'screener-content'})
		tables = div.findAll('table')
		if len(tables) < 4:
			return []
		spans = tables[3].find('tr').find('td').findAll('span')
		result = [span.text.strip() for span in spans]

		return result

	@classmethod
	def get_urls(cls):
		return [
			cls.URL_20_50[:-2],
			cls.URL_50_200[:-2],
			cls.URL_PRICE_CROSS_ABV_50[:-2],
		]
