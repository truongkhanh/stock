from bs4 import BeautifulSoup
import os
import webbrowser

from utilities.utility import Utility, OUTPUT_FOLDER


class IBD50:
	NUM_IBD_STOCK = 50
	NUM_PAGES_TO_OPEN = 10
	IBD_50_URL = 'http://research.investors.com/stock-lists/ibd-50/'
	DATA_FOLDER = 'ibd50_input'

	@classmethod
	def get_ibd_list(cls):
		"""Return a map from symbol to ibd 50 rank and vice versa"""
		result = {}
		read_files = set()

		while True:
			response = raw_input("Open IBD50 Pages (y/n)?")
			if response == 'y':
				for i in range(IBD50.NUM_PAGES_TO_OPEN):
					webbrowser.open(IBD50.IBD_50_URL)
				raw_input('Enter after you save all pages!')

			folder_path = os.path.join(OUTPUT_FOLDER, IBD50.DATA_FOLDER)
			files = os.listdir(folder_path)
			for file in files:
				if not file.endswith('.html') and not file.endswith('.htm'):
					continue

				if file in read_files:
					continue
				else:
					read_files.add(file)

				file_path = os.path.join(folder_path, file)
				html = Utility.read_file(file_path)
				result.update(IBD50Parser.get(html))

			missing_ranks = IBD50._find_rank_missing(result)
			print('Not found {}'.format(missing_ranks))
			if missing_ranks:
				response = raw_input('Do you want to continue find missing ranks (y/n)?')
				if response == 'n':
					break
			else:
				break

		return result

	@classmethod
	def _find_rank_missing(cls, ibd50_list):
		return [i + 1 for i in range(IBD50.NUM_IBD_STOCK) if (i + 1) not in ibd50_list]


class IBD50Parser:
	@classmethod
	def get(cls, html):
		bs_obj = BeautifulSoup(html, "html.parser")
		table = bs_obj.find('table', {'class': 'stockListTable'})
		rows = table.findAll('tr')[1:]

		ibd_list = {}
		for i in range(len(rows)):
			symbol = rows[i].findAll('td')[0].find('div').find('span').find('a').text.strip()
			rank = int(rows[i].findAll('td')[1].text.strip())
			ibd_list[symbol] = rank
			ibd_list[rank] = symbol

		return ibd_list
