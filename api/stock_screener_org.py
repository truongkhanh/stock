from bs4 import BeautifulSoup

from utilities.html_manager import (
	HTMLManager,
	URLRequest
)


class StockScreenerOrg(URLRequest):
	MACD_CROSS_OVER_URL = 'http://www.stock-screener.org/macd-crossover.aspx'
	MACD_CROSS_DOWN_URL = 'http://www.stock-screener.org/macd-cross-down.aspx'
	STOCHASTIC_OVER_BOUGHT_URL = 'http://www.stock-screener.org/stochastic-overbought.aspx'
	STOCHSTIC_OVER_SOLD_URL = 'http://www.stock-screener.org/stochastic-oversold.aspx'

	@classmethod
	def get_macd_cross_over(cls):
		return cls._get_symbols(StockScreenerOrg.MACD_CROSS_OVER_URL)

	@classmethod
	def get_macd_cross_down(cls):
		return cls._get_symbols(StockScreenerOrg.MACD_CROSS_DOWN_URL)

	@classmethod
	def get_stochastic_over_bought(cls):
		return cls._get_symbols(StockScreenerOrg.STOCHASTIC_OVER_BOUGHT_URL)

	@classmethod
	def get_stochastic_over_sold(cls):
		return cls._get_symbols(StockScreenerOrg.STOCHSTIC_OVER_SOLD_URL)

	@classmethod
	def _get_symbols(cls, url):
		html = HTMLManager.get_html(url)
		bs_obj = BeautifulSoup(html, "html.parser")
		table = bs_obj.find('table', {'class': 'styled'})
		table_body = table.find('tbody')
		stock_rows = table_body.findAll('tr')

		result = []
		for row in stock_rows:
			first_column = row.find('td')
			result.append(first_column.text.strip())
		return result

	@classmethod
	def get_urls(cls):
		return [
			cls.MACD_CROSS_DOWN_URL,
			cls.MACD_CROSS_OVER_URL,
			cls.STOCHASTIC_OVER_BOUGHT_URL,
			cls.STOCHSTIC_OVER_SOLD_URL,
		]
