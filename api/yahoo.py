from bs4 import BeautifulSoup
from datetime import datetime

from utilities.html_manager import (
	HTMLManager,
	URLRequest
)
from utilities.utility import Utility


class Yahoo(URLRequest):
	SUMMARY_URL = 'https://sg.finance.yahoo.com/q?s={}'
	DAILY_PRICE_URL = 'https://sg.finance.yahoo.com/q/hp?s={}'
	WEEKLY_PRICE_URL = 'https://sg.finance.yahoo.com/q/hp?s={}&g=w'

	@classmethod
	def get_price(cls, symbol):
		html = HTMLManager.get_html(cls.SUMMARY_URL.format(symbol))
		bs_obj = BeautifulSoup(html, "html.parser")
		price_span = bs_obj.find('span', {'id': 'yfs_l84_{}'.format(symbol.lower())})
		return Utility.to_float(price_span.text)

	@classmethod
	def get_price_history(cls, symbol):
		symbol = symbol.lower()
		avg_volume = cls._get_avg_volume(symbol)
		daily_prices = cls._get_daily_prices(symbol)
		weekly_prices = cls._get_weekly_prices(symbol)

		return {
			'avg_volume': avg_volume,
			'daily_prices': daily_prices,
			'weekly_prices': weekly_prices,
		}

	@classmethod
	def _get_avg_volume(cls, symbol):
		html = HTMLManager.get_html(cls.SUMMARY_URL.format(symbol))
		bs_obj = BeautifulSoup(html, "html.parser")
		table = bs_obj.find('table', {'id': 'table2'})
		volume_td = table.findAll('td', {'class': 'yfnc_tabledata1'})[3]
		return Utility.to_int(volume_td.text)

	@classmethod
	def _get_daily_prices(cls, symbol):
		"""return list of dict of [date, open, high, low, close, volume]"""
		html = HTMLManager.get_html(cls.DAILY_PRICE_URL.format(symbol))
		return cls._get_prices(BeautifulSoup(html, "html.parser"))

	@classmethod
	def _get_weekly_prices(cls, symbol):
		"""return list of dict of [date, open, high, low, close, volume]"""
		html = HTMLManager.get_html(cls.WEEKLY_PRICE_URL.format(symbol))
		return cls._get_prices(BeautifulSoup(html, "html.parser"))

	@classmethod
	def _get_prices(cls, bs_obj):
		result = []
		table = bs_obj.find('table', {'class': 'yfnc_datamodoutline1'})
		table = table.find('table')
		price_rows = table.findAll('tr')
		for row in price_rows[1:-1]:
			price_info = cls._get_price_info(row)
			if price_info:
				result.append(price_info)

		return result

	@classmethod
	def _get_price_info(cls, price_row):
		columns = price_row.findAll('td')
		if len(columns) < 6:
			return {}
		date = datetime.strptime(columns[0].text, '%d %b %Y')
		open_price = Utility.to_float(columns[1].text)
		high_price = Utility.to_float(columns[2].text)
		low_price = Utility.to_float(columns[3].text)
		close_price = Utility.to_float(columns[4].text)
		volume = Utility.to_int(columns[5].text)
		return {
			'date': date,
			'open': open_price,
			'high': high_price,
			'low': low_price,
			'close': close_price,
			'change': round((close_price - open_price) * 100.0 / open_price, 2),
			'volume': volume
		}

	@classmethod
	def get_urls(cls):
		return [
			cls.SUMMARY_URL,
			cls.DAILY_PRICE_URL,
			cls.WEEKLY_PRICE_URL,
		]
