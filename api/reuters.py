from bs4 import BeautifulSoup

from utilities.html_manager import (
	HTMLManager,
	URLRequest
)
from utilities.utility import Utility


class ReuterRank(URLRequest):
	URL = 'http://www.reuters.com/finance/stocks/analyst?symbol={}'
	DEFAULT_RANK = 9

	@classmethod
	def get(cls, symbol):
		try:
			html = HTMLManager.get_html(cls.URL.format(symbol))
			bs_obj = BeautifulSoup(html, "html.parser")
			table = bs_obj.findAll('table')[1]
			result_row = table.findAll('tr')[-1]
			value = result_row.findAll('td')[1].text.strip()
			return Utility.to_float(value)
		except:
			return ReuterRank.DEFAULT_RANK

	@classmethod
	def get_urls(cls):
		return [cls.URL]
