from bs4 import BeautifulSoup

from utilities.html_manager import (
	HTMLManager,
	URLRequest
)


class ZackRank(URLRequest):
	URL = 'https://www.zacks.com/stock/quote/{}'
	DEFAULT_RANK = 9

	@classmethod
	def get(cls, symbol):
		try:
			html = HTMLManager.get_html(cls.URL.format(symbol))
			bs_obj = BeautifulSoup(html, "html.parser")
			div = bs_obj.find('div', {'class': 'rank_container_right'})
			div = div.find('div', {'class': 'zr_rankbox'})
			spans = div.findAll('span')
			for span in spans:
				text = span.text.strip()
				if text:
					return int(text)
		except:
			pass
		return cls.DEFAULT_RANK

	@classmethod
	def get_urls(cls):
		return [cls.URL]
