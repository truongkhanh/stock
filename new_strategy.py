import argparse
from datetime import datetime

from api.finviz import FinvizScreener
from api.stock_screener_org import StockScreenerOrg
from api.stockta import StockTAMACD
from utilities.email_helper import EmailHelper
from utilities.utility import Utility


def main():
    """
    Find all stock in the list which is either godlen cross 50-200 or macd cross over
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', dest='input', help='File list of symbols')
    args = parser.parse_args()
    symbols = Utility.get_symbols_from_file(args.input)

    # golden cross
    golden_cross = FinvizScreener.get(opt=FinvizScreener.GOLDEN_CROSS_50_200_OPT, symbols=','.join(symbols))

    low_cross_over, will_low_cross_over = find_by_macd(symbols)

    if golden_cross or low_cross_over or will_low_cross_over:
        body = 'Golden Cross:\n{}\n\n\nLow Cross Over:\n{}\n\n\nWill Low Cross Over:\n{}'.format(
            golden_cross, low_cross_over, will_low_cross_over
        )
        EmailHelper.sendEmail(['truongkhanhit@gmail.com'], subject='[STOCK] To Buy', body=body, txtAttachPaths=[])


def find_by_macd(symbols):
    # macd cross over
    cross_over = StockScreenerOrg.get_macd_cross_over()
    cross_over = [symbol for symbol in symbols if symbol in cross_over]
    # check macd negative
    low_cross_over = []
    will_low_cross_over = []
    MAX_MACD_VALUE = 0
    MAX_DIFF_ALLOWED = 0.2
    for symbol in symbols:
        macd_values = StockTAMACD.get(symbol)
        if macd_values[0] < MAX_MACD_VALUE or macd_values[1] < MAX_MACD_VALUE:
            if symbol in cross_over:
                low_cross_over.append(symbol)

            if abs(macd_values[0] - macd_values[1]) < MAX_DIFF_ALLOWED:
                will_low_cross_over.append(symbol)
    return low_cross_over, will_low_cross_over

if __name__ == '__main__':
    start_time = datetime.now()
    main()
    end_time = datetime.now()
    print('Running time {}s'.format((end_time - start_time).seconds))
