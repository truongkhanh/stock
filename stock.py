import argparse
from datetime import datetime
import os

from screener.stock_screener import Screener
from technical_indicators.stock_checkup import StockCheckup
from utilities.utility import Utility
from utilities.email_helper import EmailHelper


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', '--input', dest='file', help='File list of symbols')
	parser.add_argument('-b', '--buy-signal', dest='buy_signal', action='store_true', help='Checkup buy signal')
	parser.add_argument('-s', '--sell-signal', dest='sell_signal', action='store_true', help='Checkup sell signal')
	parser.add_argument('--screener', dest='screener', action='store_true', help='Screener stocks')
	parser.add_argument('--ibd', dest='ibd', action='store_true', help='Screener ibd list')
	parser.add_argument('--ibd-url', dest='ibd_url', action='store_true', help='Get Investors.com url for symbols')
	parser.add_argument('-n', '--number-of-threads', dest='number_of_threads', type=int, default=50, help='Number of threads')
	parser.add_argument('--daily-run', dest='daily_run', action='store_true', help='Run daily, screen, find buy signal, only email when having buy signal')
	parser.add_argument('--open-dashboard', dest='open_dashboard', action='store_true', help='Open dashboard to display information')


	args = parser.parse_args()

	if args.ibd_url:
		Utility.get_investors_urls(args.file)
	elif args.screener:
		Screener.screen(args.ibd)
	elif args.buy_signal or args.sell_signal:
		StockCheckup.check(args.file, args.sell_signal, args.buy_signal, args.number_of_threads)
		dashboard_file = Utility.generate_html_page(args.file)
		os.system('open {}'.format(dashboard_file))
	elif args.daily_run:
		Screener.screen(is_run_ibd=False)
		output_file = StockCheckup.daily_buy_signal(args.file)
		dashboard_file = Utility.generate_html_page(args.file)
		EmailHelper.sendEmail(['truongkhanhit@gmail.com'], subject='Buy Signal', body='', txtAttachPaths=[output_file, dashboard_file])
	elif args.open_dashboard:
		dashboard_file = Utility.generate_html_page(args.file)
		os.system('open {}'.format(dashboard_file))


if __name__ == '__main__':
	start_time = datetime.now()
	main()
	end_time = datetime.now()
	print('Running time {}s'.format((end_time - start_time).seconds))
